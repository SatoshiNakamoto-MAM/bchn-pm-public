# BCHN PM - Public

Bitcoin Cash Node project management repository (public)

This repository is for published project management documents
Once published, PM artifacts live out the rest of their life cycle until retirement here.

Folder descriptions:

- team: information about BCHN contributors (for website)
- finance: fundraising proposals, bookkeeping
- planning: project task scheduling and backport tracking
- pr: public relations materials
